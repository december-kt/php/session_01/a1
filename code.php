<?php

	function getFullAddress($specificAddress, $city, $province, $country) {

		return "$specificAddress, $city, $province, $country";

	}

	function getLetterGrade($numericalGrade) {

		if($numericalGrade < 75) {
			return "$numericalGrade is equivalent to D";
		}
		elseif($numericalGrade >= 75 && $numericalGrade < 77) {
			return "$numericalGrade is equivalent to C-";
		}
		elseif ($numericalGrade >= 77 && $numericalGrade < 80) {
			return "$numericalGrade is equivalent to C";
		}
		elseif ($numericalGrade >= 80 && $numericalGrade < 83) {
			return "$numericalGrade is equivalent to C+";
		}
		elseif ($numericalGrade >= 83 && $numericalGrade < 86) {
			return "$numericalGrade is equivalent to B-";
		}
		elseif ($numericalGrade >= 86 && $numericalGrade < 89) {
			return "$numericalGrade is equivalent to B";
		}
		elseif ($numericalGrade >= 89 && $numericalGrade < 92) {
			return "$numericalGrade is equivalent to B+";
		}
		elseif ($numericalGrade >= 92 && $numericalGrade < 95) {
			return "$numericalGrade is equivalent to A-";
		}
		elseif ($numericalGrade >= 95 && $numericalGrade < 98) {
			return "$numericalGrade is equivalent to A";
		}
		elseif ($numericalGrade >= 98 && $numericalGrade <= 100) {
			return "$numericalGrade is equivalent to A+";
		}
		else {
			return 'Invalid Grade';
		};

	}
	
?> 
